package com.crypto.databaseservice.common.infrastructure;

import com.crypto.databaseservice.common.model.BaseEntity;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentityGenerator;

import java.io.Serializable;
import java.util.UUID;

public class CustomIdGenerator extends IdentityGenerator {

  @Override
  public Serializable generate(final SessionImplementor session, final Object obj) throws HibernateException {
    if (obj == null) {
      throw new HibernateException(new NullPointerException());
    }
    BaseEntity entity = (BaseEntity) obj;
    return entity.getId() == null ? UUID.randomUUID().toString() : entity.getId();
  }

}
