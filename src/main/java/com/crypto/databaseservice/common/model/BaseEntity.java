package com.crypto.databaseservice.common.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "CustomIdGenerator")
  @GenericGenerator(name = "CustomIdGenerator", strategy = "com.crypto.databaseservice.common.infrastructure.CustomIdGenerator")
  protected String id;

}
