package com.crypto.databaseservice.crypto.model;

import com.crypto.databaseservice.common.model.BaseEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@Table(name = "quotes")
public class Quote extends BaseEntity {

  private String username;

  private String quote;
}
