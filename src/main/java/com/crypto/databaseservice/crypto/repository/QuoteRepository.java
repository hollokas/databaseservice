package com.crypto.databaseservice.crypto.repository;

import com.crypto.databaseservice.crypto.model.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface QuoteRepository extends JpaRepository<Quote, String>{

  List<Quote> findByUsername(String username);

  Quote findByUsernameAndQuote(String username, String quote);
}
