package com.crypto.databaseservice.crypto.rest;

import com.crypto.databaseservice.crypto.dto.QuoteDTO;
import com.crypto.databaseservice.crypto.model.Quote;
import com.crypto.databaseservice.crypto.service.QuoteService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quotes")
public class QuoteRestController {

  @Autowired
  QuoteService quoteService;

  @SneakyThrows
  @GetMapping("/{username}")
  public ResponseEntity<List<String>> getUserQuotes(@PathVariable("username") final String username) {

    List<String> quotes = quoteService.getQuotes(username);

    return new ResponseEntity<>(quotes, HttpStatus.OK);
  }

  @GetMapping("/retrieveAllQuotes")
  public ResponseEntity<List<Quote>> retrieveAllQuotes() {

    List<Quote> quotes = quoteService.fetchAllQuotes();

    return new ResponseEntity<>(quotes, HttpStatus.OK);
  }

  @GetMapping("/retrieveAllUsers")
  public ResponseEntity<List<String>> retrieveAllUsernames() {

    List<String> usernames = quoteService.fetchAllUsers();

    return new ResponseEntity<>(usernames, HttpStatus.OK);

  }

  @SneakyThrows
  @PostMapping("/addQuote")
  public ResponseEntity<Void> addQuote(@RequestBody final QuoteDTO quoteDTO) {

    quoteService.addQuote(quoteDTO);

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @SneakyThrows
  @PostMapping("/deleteQuote")
  public ResponseEntity<Void> deleteQuote(@RequestBody final QuoteDTO quoteDTO) {

    quoteService.deleteQuote(quoteDTO);

    return new ResponseEntity<>(HttpStatus.OK);
  }

  @SneakyThrows
  @DeleteMapping("/deleteUserQuotes/{username}")
  public ResponseEntity<Void> deleteUserQuotes(@PathVariable("username") final String username) {

    quoteService.deleteUserQuotes(username);

    return new ResponseEntity<>(HttpStatus.OK);
  }

}
