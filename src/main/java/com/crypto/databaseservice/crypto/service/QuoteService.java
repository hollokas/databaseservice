package com.crypto.databaseservice.crypto.service;

import com.crypto.databaseservice.crypto.dto.QuoteDTO;
import com.crypto.databaseservice.crypto.model.Quote;
import com.crypto.databaseservice.crypto.repository.QuoteRepository;
import com.crypto.databaseservice.crypto.validator.QuoteValidator;
import com.crypto.databaseservice.error.model.InvalidQuoteException;
import com.crypto.databaseservice.error.model.UserNotFoundException;
import com.crypto.databaseservice.error.model.UserOrCryptoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class QuoteService {

  @Autowired
  QuoteRepository quoteRepository;

  @Autowired
  QuoteValidator quoteValidator;

  public Optional<List<Quote>> findUserByName(final String username) {

    Optional<List<Quote>> quotes = username == null ? Optional.empty()
        : quoteRepository.findByUsername(username).isEmpty() ? Optional.empty()
        : Optional.ofNullable(quoteRepository.findByUsername(username));

    return quotes;
  }

  private Optional<Quote> findQuoteByNameAndCrypto(String username, String quote) {

    Optional<Quote> result = username == null ? Optional.empty()
        : quote == null ? Optional.empty()
        : quoteRepository.findByUsernameAndQuote(username, quote) == null ? Optional.empty()
        : Optional.ofNullable(quoteRepository.findByUsernameAndQuote(username, quote));

    return result;
  }

  public void validateQuote(final QuoteDTO quoteDTO) {
    Errors errors = new BeanPropertyBindingResult(quoteDTO, "quote");
    quoteValidator.validate(quoteDTO, errors);
    if (errors.hasErrors()) {
      throw new InvalidQuoteException(errors.getAllErrors());
    }
  }

  public List<Quote> fetchAllQuotes() {

    return quoteRepository.findAll();
  }

  public List<String> fetchAllUsers() {

    Set<String> users = quoteRepository.findAll().stream()
        .map(quote -> quote.getUsername())
        .collect(Collectors.toSet());

    return new ArrayList<>(users);
  }

  public List<String> getQuotes(final String username) {

    List<String> quotes = findUserByName(username).orElseThrow(() -> new UserNotFoundException(username))
        .stream()
        .map(Quote::getQuote)
        .collect(Collectors.toList());

    return quotes;
  }

  public void addQuote(final QuoteDTO quoteDTO) {

    validateQuote(quoteDTO);

    quoteDTO.getQuotes()
        .stream()
        .forEach(
            quote -> quoteRepository.save(Quote.of(quoteDTO.getUsername(), quote))
        );

  }

  public void deleteQuote(QuoteDTO quoteDTO) {

    validateQuote(quoteDTO);

    Quote quote =
        findQuoteByNameAndCrypto(quoteDTO.getUsername(),quoteDTO.getQuotes().get(0))
          .orElseThrow(() -> new UserOrCryptoNotFoundException(quoteDTO.getUsername(), quoteDTO.getQuotes().get(0)));

    quoteRepository.delete(quote);

  }

  public void deleteUserQuotes(final String username) {

    List<Quote> quotes = findUserByName(username).orElseThrow(() -> new UserNotFoundException(username));

    quotes.forEach(quote -> quoteRepository.delete(quote));

  }
}
