package com.crypto.databaseservice.crypto.validator;

import com.crypto.databaseservice.crypto.dto.QuoteDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class QuoteValidator implements Validator{

  @Override
  public boolean supports(final Class<?> clazz) {
    return QuoteDTO.class.isAssignableFrom(clazz);
  }

  public void validate(final Object object, final Errors errors) {

    QuoteDTO quoteDTO = (QuoteDTO) object;

    if (quoteDTO.getUsername() == null) {
      errors.rejectValue("username", "Invalid username - username cannot be null");
    }

    if (quoteDTO.getQuotes().isEmpty()) {
      errors.rejectValue("quotes", "Invalid quotes list - quotes list cannot be empty");
    }

  }

}
