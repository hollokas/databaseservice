package com.crypto.databaseservice.error.handler;

import com.crypto.databaseservice.error.model.ErrorWrapper;
import com.crypto.databaseservice.error.model.InvalidQuoteException;
import com.crypto.databaseservice.error.model.UserNotFoundException;
import com.crypto.databaseservice.error.model.UserOrCryptoNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<Object> userNotFoundException(final UserNotFoundException ex, final WebRequest request) {
    ErrorWrapper errorWrapper = new ErrorWrapper();
    errorWrapper.addError("userNotFound", ex.getMessage());
    return handleExceptionInternal(ex, errorWrapper, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(UserOrCryptoNotFoundException.class)
  public ResponseEntity<Object> userOrCryptoNotFoundException(final UserNotFoundException ex, final WebRequest request) {
    ErrorWrapper errorWrapper = new ErrorWrapper();
    errorWrapper.addError("userOrCryptoNotFound", ex.getMessage());
    return handleExceptionInternal(ex, errorWrapper, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(InvalidQuoteException.class)
  public ResponseEntity<Object> handleInvalidQuoteException(final InvalidQuoteException ex, final WebRequest request) {
    ErrorWrapper errorWrapper = new ErrorWrapper();
    ex.getErrors().forEach(error -> errorWrapper.addError(error));
    return handleExceptionInternal(ex, errorWrapper, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }

}
