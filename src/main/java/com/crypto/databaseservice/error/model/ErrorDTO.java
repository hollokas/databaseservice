package com.crypto.databaseservice.error.model;

import lombok.Data;

@Data
public class ErrorDTO {

  private final String code;

  private final String reason;
}
