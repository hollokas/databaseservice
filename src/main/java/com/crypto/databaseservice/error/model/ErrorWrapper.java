package com.crypto.databaseservice.error.model;

import lombok.Data;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorWrapper {

  private List<ErrorDTO> errors = new ArrayList<>();

  public void addError(ObjectError error) {
    if (error instanceof FieldError) {
      FieldError fieldError = (FieldError) error;
      addError(fieldError.getField(), fieldError.getCode());
    } else {
      addError(error.getCode(), error.getDefaultMessage());
    }
  }

  public void addError(String code, String message) {
    errors.add(new ErrorDTO(code, message));
  }

}


