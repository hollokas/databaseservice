package com.crypto.databaseservice.error.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.ObjectError;

import java.util.Arrays;
import java.util.List;

@Data
@AllArgsConstructor
public class InvalidQuoteException extends RuntimeException {

  private final List<ObjectError> errors;

  public InvalidQuoteException(final String code, final String message) {
    this.errors = Arrays.asList(new ObjectError(code, message));
  }

}
