package com.crypto.databaseservice.error.model;

import lombok.Data;

@Data
public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(String username) {
    super("User with the name " + username + " was not found");
  }
}
