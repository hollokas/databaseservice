package com.crypto.databaseservice.error.model;

import lombok.Data;

@Data
public class UserOrCryptoNotFoundException extends RuntimeException {

  public UserOrCryptoNotFoundException(String username, String s) { super("User with the name " + username + " or crypto with name " + s + " was not found");
  }
}
